#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


export DEB_BUILD_MAINT_OPTIONS   := hardening=+bindnow
# CMake doesn't use CPPFLAGS, pass them to CFLAGS/CXXFLAGS to enable the
# missing (hardening) flags. dpkg_buildflags is necessary because $(shell ..)
# doesn't use local environment variables. (bug #665324)
dpkg_buildflags = DEB_BUILD_MAINT_OPTIONS=$(DEB_BUILD_MAINT_OPTIONS) dpkg-buildflags
export DEB_CFLAGS_MAINT_APPEND   := -pipe -Wall $(shell $(dpkg_buildflags) --get CPPFLAGS)
export DEB_CXXFLAGS_MAINT_APPEND := -pipe -Wall $(shell $(dpkg_buildflags) --get CPPFLAGS)

#export DEB_LDFLAGS_MAINT_APPEND  := -Wl,-z,defs -Wl,--as-needed
# disable -Wl,--as-needed on ia64 due to bug #718047, see also #725143
ifeq ($(DEB_HOST_ARCH),ia64)
export DEB_LDFLAGS_MAINT_APPEND  := -Wl,-z,defs
else
export DEB_LDFLAGS_MAINT_APPEND  := -Wl,-z,defs -Wl,--as-needed
endif

# Reduce debug info to workaround address space limitation
ifneq (,$(filter $(DEB_HOST_ARCH), mipsel))
    export DEB_CXXFLAGS_MAINT_APPEND += -g1
endif

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)


# Use this variable to define the particular version of OGRE that we're building
OGRE_SOVERSION=1.12.10

OGRE_CHANGELOG = Docs/ChangeLog.md

# Directories to install with Multi-arch and versioned OGRE dir names
LIB_MULTIARCH_DIR = /usr/lib/$(DEB_HOST_MULTIARCH)


%:
	dh $@ \
		--buildsystem=cmake \
		--with python3

override_dh_auto_configure:
        # RPATH from http://www.ogre3d.org/tikiwiki/Building+Ogre+With+CMake
	dh_auto_configure -- \
		-DOGRE_LIB_DIRECTORY="lib/$(DEB_HOST_MULTIARCH)" \
		-DCMAKE_SKIP_RPATH:BOOL=TRUE \
		-DOGRE_BUILD_RENDERSYSTEM_GLES2:BOOL=TRUE \
		-DOGRE_BUILD_DEPENDENCIES:BOOL=FALSE \
		-DOGRE_BUILD_DOCS:BOOL=TRUE \
		-DOGRE_INSTALL_DOCS:BOOL=TRUE \
		-DOGRE_BUILD_SAMPLES:BOOL=FALSE \
		-DOGRE_INSTALL_SAMPLES:BOOL=FALSE \
		-DOGRE_INSTALL_SAMPLES_SOURCE:BOOL=FALSE \
		-DOGRE_BUILD_COMPONENT_PYTHON:BOOL=TRUE \
		-DOGRE_BUILD_COMPONENT_CSHARP:BOOL=FALSE \
		-DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3

#override_dh_auto_build-arch:
#	dh_auto_build -a --

override_dh_auto_build-indep:
	dh_auto_build -i --

        # generate doxygen API doc
        #cd obj-$(DEB_HOST_MULTIARCH) && make OgreDoc
	dh_auto_build -i -- OgreDoc

override_dh_install-arch:
        # Copy files from template for this particular version
	cp -f debian/libogreVERSION.install debian/libogre$(OGRE_SOVERSION)t64.install
	cp -f debian/libogreVERSION.lintian-overrides debian/libogre$(OGRE_SOVERSION)t64.lintian-overrides

        # docs installed in other way
        #rm -rfv debian/tmp/usr/share/OGRE/docs

        # remove .md5, .map and other files that we do not want installed
        #find debian/tmp/usr/share/OGRE/docs/api/html -type f -not \( -iname '*.html' -or -iname '*.png' -or -iname '*.css' -or -iname '*.js' \) -exec rm -fv {} \;

	dh_install -a

override_dh_dwz:
	dh_dwz -XlibOgreMain.so.1.12.10 -XPlugin_OctreeZone.so.1.12.10

override_dh_install-indep:
        # remove .md5, .map and other files that we do not want installed
	find debian/tmp/usr/share/doc/OGRE/api/html -type f -not \( -iname '*.html' -or -iname '*.png' -or -iname '*.css' -or -iname '*.js' \) -exec rm -fv {} \;
	
	#remove latex docs
	rm -rfv debian/tmp/usr/share/doc/OGRE/api/latex
	
	#remove temp files
	rm -rfv debian/tmp/usr/share/doc/OGRE/api/bib*.aux
	rm -rfv debian/tmp/usr/share/doc/OGRE/api/citelist.doc*

	dh_install -i -Xjquery.js

override_dh_installchangelogs:
	dh_installchangelogs -k $(OGRE_CHANGELOG)

override_dh_clean:
	dh_clean

        # Remove files from template
	rm -rf debian/libogre$(OGRE_SOVERSION).*

# For new symbols when compiled with GCC 7
override_dh_makeshlibs:
	dh_makeshlibs -V"libogre1.12.10t64 (>= 1.12.10+dfsg1-1~)"


override_dh_shlibdeps:
	dh_shlibdeps -l/usr/lib/$(DEB_HOST_MULTIARCH)/OGRE/

override_dh_missing:
	dh_missing --list-missing
